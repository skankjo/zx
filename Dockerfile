FROM azul/zulu-openjdk-debian:17
RUN apt-get update
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get install --quiet --assume-yes git curl zip unzip
RUN curl -s "https://get.sdkman.io" | bash
RUN chmod a+x $HOME/.sdkman/bin/sdkman-init.sh
RUN source $HOME/.sdkman/bin/sdkman-init.sh && sdk install gradle
COPY . $HOME/zx
WORKDIR $HOME/zx
