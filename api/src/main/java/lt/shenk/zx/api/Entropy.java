package lt.shenk.zx.api;

public record Entropy(double entropy) implements Foldable<Entropy>, Comparable<Entropy> {

    public static final Entropy EMPTY = new Entropy(0d);

    @Override
    public int compareTo(Entropy o) {
        return Double.compare(entropy, o.entropy);
    }

    @Override
    public Entropy concat(Entropy other) {
        return new Entropy(entropy + other.entropy());
    }

    public static Monoid<Entropy> monoid() {
        return Monoid.of(EMPTY, Entropy::concat);
    }
}
