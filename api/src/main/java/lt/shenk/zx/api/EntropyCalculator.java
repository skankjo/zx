package lt.shenk.zx.api;

import java.util.List;

public interface EntropyCalculator {
    Entropy calculate(int wordLen, List<? extends Match> matches);
}
