package lt.shenk.zx.api;

public interface Foldable<V> {
    V concat(V other);
}
