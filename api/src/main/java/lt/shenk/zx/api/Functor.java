package lt.shenk.zx.api;

import java.util.function.UnaryOperator;

public interface Functor<V> {
    Functor<V> map(UnaryOperator<V> f);
}
