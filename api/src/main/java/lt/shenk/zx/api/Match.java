package lt.shenk.zx.api;

public interface Match {
    Word word();
    Entropy entropy();
    int len();
    Token token();
}
