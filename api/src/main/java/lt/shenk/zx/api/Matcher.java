package lt.shenk.zx.api;

import java.util.stream.Stream;

public interface Matcher {
    Stream<? extends Match> match(Word word);
}
