package lt.shenk.zx.api;

import java.util.stream.Stream;

public interface Matching {
    Stream<? extends Match> matches(Word word);
}
