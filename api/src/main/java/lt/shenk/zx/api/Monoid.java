package lt.shenk.zx.api;

public interface Monoid<T> extends Semigroup<T> {
    T zero();

    public static <T> Monoid<T> of(T zero, Semigroup<T> semigroup) {
        return new Monoid<T>() {
            @Override
            public T zero() {
                return zero;
            }

            @Override
            public T apply(T t, T t2) {
                return semigroup.apply(t, t2);
            }
        };
    }
}
