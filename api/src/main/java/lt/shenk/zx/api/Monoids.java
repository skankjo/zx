package lt.shenk.zx.api;

public interface Monoids {
    static Monoid<Integer> intSum() {
       return Monoid.of(0, Integer::sum);
    };

    static Monoid<Integer> intMult() {
        return Monoid.of(1, (i, j) -> i * j);
    }
}
