package lt.shenk.zx.api;

public interface Range {
    int i();
    int j();
    String substring(String s);
    int len();
}
