package lt.shenk.zx.api;

import java.util.function.BinaryOperator;

@FunctionalInterface
public interface Semigroup<T> extends BinaryOperator<T> {
}
