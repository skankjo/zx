package lt.shenk.zx.api;

public interface Token {
    String token();
    Range range();
}
