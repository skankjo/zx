package lt.shenk.zx.api;

import java.util.stream.Stream;

public interface Word {
    Stream<Token> tokens();
    String word();
    default double entropy() {
        return 0d;
    }
}
