package lt.shenk.zx.bin;

import lt.shenk.zx.api.Word;
import lt.shenk.zx.core.App;
import lt.shenk.zx.core.Result;
import lt.shenk.zx.core.Tokens;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        String psw = Stream.of(args).collect(Collectors.joining(" "));
        Word word = new Tokens(psw);

        long start = System.currentTimeMillis();
        Result result = new App().run(word);
        System.out.println(String.format("Time elapsed: %d ms", System.currentTimeMillis() - start));

        result.matches().forEach(System.out::println);

        System.out.println(String.format("Entropy: %f", result.entropy()));
        System.out.println(String.format("Guesses: %f", result.guesses()));
        System.out.println(String.format("Basic score: %d", result.basicScore()));
    }
}
