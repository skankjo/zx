import lt.shenk.zx.api.Matcher;

module app {
    requires api;
    requires core;
    uses Matcher;
}