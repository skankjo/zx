package lt.shenk.zx.core;

import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Word;

import java.util.List;

public class App {
    public Result run(Word word) {
        List<? extends Match> matches = new OmniMatching().matches(word).toList();
        return new Result(word, matches);
    }
}
