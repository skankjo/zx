package lt.shenk.zx.core;

import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.api.Matching;
import lt.shenk.zx.api.Word;

import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OmniMatching implements Matching {

    @Override
    public Stream<? extends Match> matches(Word word) {
        return matchers().stream()
                .flatMap(matcher -> matcher.match(word));
    }

    private static List<Matcher> matchers() {
        ServiceLoader<Matcher> matchers = ServiceLoader.load(Matcher.class);
        return matchers.stream()
                .map(ServiceLoader.Provider::get)
                .collect(Collectors.toList());
    }
}
