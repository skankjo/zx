package lt.shenk.zx.core;

import lt.shenk.zx.api.Range;
import lt.shenk.zx.api.Token;

import java.util.Objects;

public record RangedToken(String token, Range range) implements Token {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RangedToken that = (RangedToken) o;
        return token.equals(that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }
}
