package lt.shenk.zx.core;

import lt.shenk.zx.api.Range;

public record RegularRange(int i, int j) implements Range {
    @Override
    public String substring(String s) {
        return s.substring(i, j);
    }

    @Override
    public int len() {
        return j - i;
    }
}
