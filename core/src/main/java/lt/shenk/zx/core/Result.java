package lt.shenk.zx.core;

import lt.shenk.zx.api.EntropyCalculator;
import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Word;

import java.util.List;
import java.util.ServiceLoader;

public class Result {
    private final List<? extends Match> matches;
    private final double entropy;
    private final int wordLen;
    private final EntropyCalculator entropyCalculator = load();

    public Result(Word word, List<? extends Match> matches) {
        wordLen = word.word().length();
        this.matches = matches;
        this.entropy = calculateEntropy();
    }

    public double calculateEntropy() {
        return entropyCalculator.calculate(wordLen, matches).entropy();
    }

    public double guesses() {
        final Double guesses = Math.pow(2, this.entropy);
        return guesses.isInfinite() ? Double.MAX_VALUE : guesses;
    }

    public int basicScore() {
        record Score(double guesses, int score) {};

        List<Score> scores = List.of(
                new Score(1e3, 0),
                new Score(1e6, 1),
                new Score(1e8, 2),
                new Score(1e10, 3)
            );

        double guesses = guesses();

        return scores.stream()
                .filter(score -> guesses < score.guesses)
                .map(Score::score)
                .findFirst()
                .orElse(4);
    }

    public List<? extends Match> matches() {
        return matches;
    }

    public double entropy() {
        return this.entropy;
    }

    private static EntropyCalculator load() {
        ServiceLoader<EntropyCalculator> serviceLoader = ServiceLoader.load(EntropyCalculator.class);
        return serviceLoader.stream()
                .map(ServiceLoader.Provider::get)
                .findFirst()
                .orElseThrow(() -> new ServiceImplementationNotFoundException("Service implementation of Entropy is not found."));
    }

}
