package lt.shenk.zx.core;

public class ServiceImplementationNotFoundException extends RuntimeException {

    public ServiceImplementationNotFoundException(String message) {
        super(message);
    }
}
