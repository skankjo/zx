package lt.shenk.zx.core;

import lt.shenk.zx.api.Token;
import lt.shenk.zx.api.Word;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public record Tokens(String word) implements Word {
    public Stream<Token> tokens() {
        int len = word.length();
        return IntStream.rangeClosed(0, len)
                .mapToObj(i -> i)
                .flatMap(i -> IntStream.rangeClosed(i, len).mapToObj(j -> new RegularRange(i, j)) )
                .filter(range -> range.i() != range.j())
                .map(range -> new RangedToken(
                        word.substring(range.i(), range.j()),
                        range
                ));
    }

}
