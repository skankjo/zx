import lt.shenk.zx.api.EntropyCalculator;
import lt.shenk.zx.api.Matcher;

module core {
    exports lt.shenk.zx.core;
    requires transitive api;
    uses Matcher;
    uses EntropyCalculator;
}