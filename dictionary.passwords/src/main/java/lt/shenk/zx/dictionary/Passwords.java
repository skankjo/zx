package lt.shenk.zx.dictionary;

import lt.shenk.zx.api.Range;
import lt.shenk.zx.dictionary.api.Dictionary;
import lt.shenk.zx.api.Token;
import lt.shenk.zx.dictionary.api.Rank;
import lt.shenk.zx.dictionary.api.RankedToken;

import java.io.InputStream;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Passwords implements Dictionary {

    private final Map<String, Optional<Integer>> tokens;

    public Passwords() {
        InputStream is = this.getClass().getModule().getClassLoader().getResourceAsStream("passwords.txt");
        record RankToken(String token, Integer rank) {}
        Scanner scanner = new Scanner(is);
        AtomicInteger i = new AtomicInteger(1);
        tokens = scanner.useDelimiter("\n")
                .tokens()
                .map(token -> token.split("\\s+"))
                .map(arr -> arr[0])
                .map(w -> new RankToken(w, i.getAndIncrement()))
                .collect(Collectors.toMap(rank -> rank.token(), rank -> Optional.of(rank.rank())));
    }

    @Override
    public Optional<RankedToken> lookup(Token token) {
        return tokens.getOrDefault(token.token().toLowerCase(), Optional.empty())
                .map(rank -> new RankedTokenRecord(token.token(), token.range(), new RankRecord(rank)));
    }

    public record RankRecord(int rank) implements Rank {}

    public record RankedTokenRecord(String token, Range range, Rank rank) implements RankedToken {}

}
