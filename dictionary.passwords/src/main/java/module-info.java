import lt.shenk.zx.dictionary.api.Dictionary;
import lt.shenk.zx.dictionary.Passwords;

module dictionary.passwords {
    requires core;
    requires dictionary;
    provides Dictionary with Passwords;
}