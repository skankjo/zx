package lt.shenk.zx.dictionary.api;

import lt.shenk.zx.api.Token;

import java.util.Optional;

public interface Dictionary {
    Optional<RankedToken> lookup(Token token);
}
