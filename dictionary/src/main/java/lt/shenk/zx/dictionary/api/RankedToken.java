package lt.shenk.zx.dictionary.api;

import lt.shenk.zx.api.Token;

public interface RankedToken extends Token {
    Rank rank();
}
