package lt.shenk.zx.entropy;

import lt.shenk.zx.api.Entropy;
import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Monoid;
import lt.shenk.zx.api.Token;
import lt.shenk.zx.api.Monoids;
import lt.shenk.zx.api.Word;

import java.util.List;
import java.util.stream.Stream;

public record CombinedMatch(Word word, List<Match> matches) implements Match {
    public static CombinedMatch concat(Match match, Match addend) {
        return new CombinedMatch(match.word(), Stream.concat(Stream.of(match), flatten(addend)).toList());
    }

    private static Stream<Match> flatten(Match addend) {
        if (addend instanceof CombinedMatch combinedMatch) {
            return combinedMatch.matches().stream()
                    .flatMap(CombinedMatch::flatten);
        }
        return Stream.of(addend);
    }

    @Override
    public Entropy entropy() {
        Monoid<Entropy> entropyMonoid = Entropy.monoid();
        return matches.stream()
                .map(Match::entropy)
                .reduce(entropyMonoid.zero(), entropyMonoid::apply);
    }

    @Override
    public int len() {
        Monoid<Integer> sum = Monoids.intSum();
        return matches.stream().map(Match::len).reduce(sum.zero(), sum::apply);
    }

    @Override
    public Token token() {
        //TODO: implement token `concat` method if there is any need of it
//        return matches.stream()
//                .map(Match::token)
//                .reduce(Token::concat);
        return null;
    }
}
