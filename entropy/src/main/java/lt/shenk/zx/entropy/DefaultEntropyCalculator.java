package lt.shenk.zx.entropy;

import lt.shenk.zx.api.Entropy;
import lt.shenk.zx.api.EntropyCalculator;
import lt.shenk.zx.api.Match;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DefaultEntropyCalculator implements EntropyCalculator {

    @Override
    public Entropy calculate(int wordLen, List<? extends Match> matches) {
        List<List<Match>> list = matches.stream()
                .collect(new MatchCollector(wordLen));
        list.forEach(System.out::println);
        System.out.println("================================");

        List<Match> combinedMatches = list.stream()
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .flatMap(this.combineMatches(wordLen, list))
                .filter(match -> match.len() == wordLen)
                .sorted(Comparator.comparing(Match::entropy))
                .toList();

        combinedMatches.stream()
                .forEach(m -> System.out.println(String.format("%f %s", m.entropy().entropy(), m)));
        System.out.println("================================");

        return combinedMatches.stream().findFirst().map(Match::entropy).orElse(Entropy.monoid().zero());
    }

    private Function<Match, Stream<? extends Match>> combineMatches(int wordLen, List<List<Match>> list) {
        return match -> {
            int j = match.token().range().j();
            int listSize = list.size();

            if (j == wordLen || j >= listSize) {
                return Stream.of(match);
            }

            List<Match> matches = IntStream.range(j, listSize)
                    .mapToObj(list::get)
                    .filter(Objects::nonNull)
                    .flatMap(this.digest(match, wordLen, list))
                    .toList();

            if (!matches.isEmpty()) {
                return matches.stream();
            }

            return Stream.of(match);
        };
    }

    private Function<List<Match>, Stream<? extends Match>> digest(Match match, int wordLen, List<List<Match>> list) {
        return matches -> matches.stream()
                .flatMap(this.combine(match, wordLen, list));
    }

    private Function<? super Match, Stream<? extends Match>> combine(Match match, int wordLen, List<List<Match>> list) {
        return secondMatch ->
                combineMatches(wordLen, list).apply(secondMatch)
                        .map(m -> CombinedMatch.concat(match, m));
    }
}
