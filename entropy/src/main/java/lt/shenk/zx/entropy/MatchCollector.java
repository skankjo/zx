package lt.shenk.zx.entropy;

import lt.shenk.zx.api.Match;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MatchCollector implements Collector<Match, List<List<Match>>, List<List<Match>>> {

    private final int len;

    public MatchCollector(int len) {
        this.len = len;
    }

    @Override
    public Supplier<List<List<Match>>> supplier() {
        List<List<Match>> arr = IntStream.range(0, len).mapToObj(i -> (List<Match>) null).toList();
        return () -> new ArrayList<>(arr);
    }

    @Override
    public BiConsumer<List<List<Match>>, Match> accumulator() {
        return (acc, item) -> {
          int i = item.token().range().i();
          Optional.ofNullable(acc.get(i))
                  .ifPresentOrElse(arr -> arr.add(item), () -> acc.set(i, new ArrayList<>(List.of(item))));
        };
    }

    @Override
    public BinaryOperator<List<List<Match>>> combiner() {
        return (arr1, arr2) -> Stream.concat(arr1.stream(), arr2.stream()).toList();
    }

    @Override
    public Function<List<List<Match>>, List<List<Match>>> finisher() {
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of();
    }
}
