import lt.shenk.zx.api.EntropyCalculator;
import lt.shenk.zx.entropy.DefaultEntropyCalculator;

module entropy {
    requires api;
    provides EntropyCalculator with DefaultEntropyCalculator;
}