package lt.shenk.zx.matcher.bruteforce;

import lt.shenk.zx.api.Entropy;
import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.core.RangedToken;

import java.util.EnumMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public record BruteforceMatch(Word word, RangedToken token) implements Match {

    private final static List<CharacterCardinality> characterCardinalities;

    private enum Cardinality {
        UPPER_CASE(26),
        LOWER_CASE(26),
        DIGIT(10),
        PUNCTUATION(31);

        private final int cardinality;

        Cardinality(int cardinality) {
            this.cardinality = cardinality;
        }

        int cardinality() {
            return cardinality;
        }
    }

    static {
        characterCardinalities = List.of(
                new CharacterCardinality(Character::isUpperCase, Cardinality.UPPER_CASE),
                new CharacterCardinality(Character::isLowerCase, Cardinality.LOWER_CASE),
                new CharacterCardinality(Character::isDigit, Cardinality.DIGIT),
                new CharacterCardinality(ch ->
                        !(Character.isUpperCase(ch) && Character.isLowerCase(ch) && Character.isDigit(ch)),
                        Cardinality.PUNCTUATION)
        );
    }

    @Override
    public Entropy entropy() {
        EnumMap<Cardinality, Boolean> foundCardinalities = new EnumMap<>(Cardinality.class);
        int cardinality = token.token().chars()
                .mapToObj(i -> (char) i)
                .map(this::findCardinality)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet())
                .stream()
                .map(Cardinality::cardinality)
                .reduce(0, Integer::sum);
        return new Entropy(log2(Math.pow(cardinality, token.token().length())));
    }

    private Optional<Cardinality> findCardinality(Character ch) {
        return characterCardinalities.stream()
                .filter(cc -> cc.character.test(ch))
                .map(CharacterCardinality::cardinality)
                .findFirst();
    }

    @Override
    public int len() {
        return token.token().length();
    }

    private static double log2(double n) {
        return Math.log(n) / Math.log(2d);
    }

    private record CharacterCardinality(Predicate<Character> character, Cardinality cardinality) {}
}
