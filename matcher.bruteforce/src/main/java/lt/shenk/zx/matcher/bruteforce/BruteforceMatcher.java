package lt.shenk.zx.matcher.bruteforce;

import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.core.RangedToken;

import java.util.stream.Stream;

public class BruteforceMatcher implements Matcher {
    @Override
    public Stream<? extends Match> match(Word word) {
        return word.tokens()
                .map(t -> (RangedToken) t)
                .map(token -> new BruteforceMatch(word, token));
    }
}
