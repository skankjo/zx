import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.matcher.bruteforce.BruteforceMatcher;

module matcher.bruteforce {
    requires core;

    provides Matcher with BruteforceMatcher;
}