package lt.shenk.zx.matcher.dictionary;

import lt.shenk.zx.api.Entropy;
import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.dictionary.api.RankedToken;
import lt.shenk.zx.matcher.dictionary.utils.Calculations;

import java.util.Objects;

public record DictionaryMatch(Word word, RankedToken token) implements Match {
    @Override
    public Entropy entropy() {
        return new Entropy(Calculations.log2(token.rank().rank())
                + Calculations.upperCaseEntropy(word.word())
                + Calculations.l33tEntropy(word.word(), token)
                + word.entropy());
    }

    @Override
    public int len() {
        return token.range().len();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DictionaryMatch that = (DictionaryMatch) o;
        return token.rank().equals(that.token.rank());
    }

    @Override
    public int hashCode() {
        return Objects.hash(token.rank());
    }
}
