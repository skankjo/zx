package lt.shenk.zx.matcher.dictionary;

import lt.shenk.zx.api.Token;
import lt.shenk.zx.dictionary.api.Dictionary;
import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.dictionary.api.Rank;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.dictionary.api.RankedToken;

import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DictionaryMatcher implements Matcher {

    protected final List<Dictionary> dictionaries;

    public DictionaryMatcher() {
        this.dictionaries = dictionaries();
    }

    @Override
    public Stream<? extends Match> match(Word word) {
        return dictionaries.stream()
                .flatMap(lookup(word))
                .map(t -> (RankedToken) t)
                .map(token -> new DictionaryMatch(word, token));
    }

    private Function<Dictionary, Stream<? extends Token>> lookup(Word word) {
        return dictionary -> word.tokens()
                .map(token -> dictionary.lookup(token))
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    private static List<Dictionary> dictionaries() {
        ServiceLoader<Dictionary> services = ServiceLoader.load(Dictionary.class);
        return services.stream()
                .map(ServiceLoader.Provider::get)
                .collect(Collectors.toList());
    }
}
