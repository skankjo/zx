package lt.shenk.zx.matcher.dictionary.utils;

import lt.shenk.zx.api.Range;
import lt.shenk.zx.api.Token;

import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Calculations {

    private static final double LOG2 = Math.log(2d);

    private Calculations() {}

    public static long nCk(final long n, final long k) {
        if (k > n) {
            return 0;
        }

        return LongStream.rangeClosed(1, k)
                .reduce(1, (acc, i) -> {
                    acc *= n - (i - 1);
                    acc /= i;
                    return acc;
                });
    }

    public static double log2(double n) {
        return Math.log(n) / LOG2;
    }

    public static double upperCaseEntropy(String s) {
        final long capsCount = Strings.countCapitalized(s);
        final long lowerCount = Strings.countLowerCase(s);
        final long possibilities = possibilities(capsCount, lowerCount);

        return Math.max(log2(possibilities), 1);
    }

    private static long possibilities(long firstCount, long secondCount) {
        final long total = firstCount + secondCount;
        final long min = Math.min(firstCount, secondCount);

        final long possibilities = LongStream.rangeClosed(0, min)
                .reduce(0, (acc, i) -> acc += nCk(total, i));
        return possibilities;
    }

    public static double l33tEntropy(String word, Token token) {
        Range range = token.range();
        String w = range.substring(word);
        char[] source = w.toLowerCase().toCharArray();
        char[] found = token.token().toCharArray();

        final long diffCount = LongStream.range(0, source.length)
                .map(i -> source[(int) i] != found[(int) i] ? 1 : 0)
                .reduce(0, Long::sum);
        final long overlappedCount = source.length - diffCount;
        final long possibilities = possibilities(diffCount, overlappedCount);
        return Math.max(1, log2(possibilities));
    }
}
