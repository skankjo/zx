package lt.shenk.zx.matcher.dictionary.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Strings {
    private Strings() {}

    public static boolean allCaps(String s) {
        return s.toUpperCase().equals(s);
    }

    public static boolean allLower(String s) {
        return s.toLowerCase().equals(s);
    }

    public static boolean firstCharCapitalized(String s) {
        return Character.isUpperCase(s.charAt(0));
    }

    public static boolean lastCharCapitalized(String s) {
        return Character.isUpperCase(s.charAt(s.length()-1));
    }

    public static long countCapitalized(String s) {
        return s.chars().mapToObj(i -> Character.valueOf((char) i))
                .filter(Character::isUpperCase)
                .count();
    }

    public static long countLowerCase(String s) {
        return s.chars().mapToObj(i -> Character.valueOf((char) i))
                .filter(Character::isLowerCase)
                .count();
    }

    public static String reverse(String s) {
        return Arrays.stream(s.split(""))
                .collect(doReverse())
                .reduce("", String::concat);
    }

    private static <T> Collector<T, ?, Stream<T>> doReverse() {
        return Collectors.collectingAndThen(Collectors.toList(), list -> {
            Collections.reverse(list);
            return list.stream();
        });
    }
}
