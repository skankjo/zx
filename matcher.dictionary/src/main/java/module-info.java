import lt.shenk.zx.dictionary.api.Dictionary;
import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.matcher.dictionary.DictionaryMatcher;

module matcher.dictionary {
    requires transitive core;
    requires transitive dictionary;
    provides Matcher with DictionaryMatcher;
    uses Dictionary;
    exports lt.shenk.zx.matcher.dictionary;
    exports lt.shenk.zx.matcher.dictionary.utils;
}