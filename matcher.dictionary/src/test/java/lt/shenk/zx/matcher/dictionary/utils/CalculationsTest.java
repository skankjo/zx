package lt.shenk.zx.matcher.dictionary.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculationsTest {

    @Test
    void shouldCalculateBinomialCoefficients() {
        //given
        long result = Calculations.nCk(100, 2);

        //then
        Assertions.assertEquals(4950, result);
    }
}
