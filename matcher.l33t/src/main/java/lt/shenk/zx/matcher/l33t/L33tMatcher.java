package lt.shenk.zx.matcher.l33t;

import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.matcher.dictionary.DictionaryMatcher;

import java.util.stream.Stream;

public class L33tMatcher extends DictionaryMatcher {
    public L33tMatcher() {
        super();
    }

    @Override
    public Stream<? extends Match> match(Word word) {
        return super.match(L33tWord.from(word));
    }
}
