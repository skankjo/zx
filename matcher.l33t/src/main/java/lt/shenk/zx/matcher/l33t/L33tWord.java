package lt.shenk.zx.matcher.l33t;

import lt.shenk.zx.api.Token;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.core.Tokens;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

public record L33tWord(String word) implements Word {
    private static Map<Character, List<String>> table = new HashMap<>();

    static {
        table.put('4', List.of("a"));
        table.put('@', List.of("a"));
        table.put('8', List.of("b"));
        table.put('(', List.of("c"));
        table.put('{', List.of("c"));
        table.put('[', List.of("c"));
        table.put('<', List.of("c"));
        table.put('3', List.of("e"));
        table.put('6', List.of("g"));
        table.put('9', List.of("g"));
        table.put('1', List.of("i", "l"));
        table.put('|', List.of("i", "l"));
        table.put('!', List.of("i"));
        table.put('7', List.of("l", "t"));
        table.put('0', List.of("o"));
        table.put('$', List.of("s"));
        table.put('5', List.of("s"));
        table.put('+', List.of("t"));
        table.put('%', List.of("x"));
        table.put('2', List.of("z"));

    }

    public static L33tWord from(Word word) {
        return new L33tWord(word.word());
    }

    @Override
    public Stream<Token> tokens() {
        return word.toLowerCase().chars().mapToObj(i -> Character.valueOf((char)i))
                .map(ch -> table.getOrDefault(ch, List.of(ch.toString())))
                .reduce(appendAllPossibleChars())
                .get()
                .stream().filter(not(word::equals))
                .map(Tokens::new)
                .flatMap(Tokens::tokens)
                .distinct()
                .filter(token -> token.token().length() > 1);
    }

    private BinaryOperator<List<String>> appendAllPossibleChars() {
        return (acc, nextChars) -> acc.stream().flatMap(
                    s -> nextChars.stream().map(next -> s.concat(next))
                ).toList();
    }
}
