import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.matcher.l33t.L33tMatcher;

module matcher.l33t {
    requires matcher.dictionary;

    provides Matcher with L33tMatcher;
}