package lt.shenk.zx.matcher.l33t;

import lt.shenk.zx.api.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

public class L33tWordTest {
    @Test
    void shouldGiveAllPossibleTokens() {
        //given
        L33tWord word = new L33tWord("a@");
        List<Token> tokens = word.tokens().toList();
        tokens.forEach(System.out::println);

        int len = word.word().length();

        //then
        Assertions.assertEquals(1, tokens.size());
        Assertions.assertEquals("aa", tokens.get(0).token());
    }

}
