package lt.shenk.zx.matcher.reversed;

import lt.shenk.zx.api.Match;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.matcher.dictionary.DictionaryMatcher;

import java.util.stream.Stream;

public class ReversedMatcher extends DictionaryMatcher {

    public ReversedMatcher() {
        super();
    }

    @Override
    public Stream<? extends Match> match(Word word) {
        return super.match(ReversedWord.from(word));
    }
}
