package lt.shenk.zx.matcher.reversed;

import lt.shenk.zx.api.Range;
import lt.shenk.zx.matcher.dictionary.utils.Strings;

public record ReversedRange(int i, int j) implements Range {
    @Override
    public String substring(String s) {
        return Strings.reverse(s.substring(i, j));
    }

    @Override
    public int len() {
        return j - i;
    }
}
