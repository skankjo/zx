package lt.shenk.zx.matcher.reversed;

import lt.shenk.zx.api.Token;
import lt.shenk.zx.api.Word;
import lt.shenk.zx.core.RangedToken;
import lt.shenk.zx.matcher.dictionary.utils.Strings;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public record ReversedWord(String word) implements Word {

    @Override
    public Stream<Token> tokens() {
        int len = word.length();
        String reversedWord = Strings.reverse(word).toLowerCase();

        return IntStream.rangeClosed(0, len)
                .mapToObj(i -> i)
                .flatMap(i -> IntStream.rangeClosed(i, len).mapToObj(j -> new ReversedRange(i, j)) )
                .filter(range -> range.i() != range.j())
                .map(range -> new RangedToken(
                        reversedWord.substring(range.i(), range.j()),
                        new ReversedRange(len - range.j(), len - range.i())
                ));
    }

    public static ReversedWord from(Word word) {
        return new ReversedWord(word.word());
    }

    @Override
    public double entropy() {
        return 1d;
    }
}
