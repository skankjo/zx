import lt.shenk.zx.api.Matcher;
import lt.shenk.zx.matcher.reversed.ReversedMatcher;

module matcher.reversed {
    requires matcher.dictionary;
    provides Matcher with ReversedMatcher;
}