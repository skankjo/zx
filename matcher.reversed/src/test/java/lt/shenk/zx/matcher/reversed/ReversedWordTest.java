package lt.shenk.zx.matcher.reversed;

import lt.shenk.zx.api.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

public class ReversedWordTest {

    @Test
    void shouldGiveAllTheTokensOfReversedWord() {
        //when
        ReversedWord word = new ReversedWord("abcdef");

        List<Token> tokens = word.tokens().toList();

        tokens.stream().forEach(System.out::println);

        Assertions.assertEquals(calculateCountOfVariants(word.word().length()), tokens.size());
    }

    private int calculateCountOfVariants(int len) {
        return IntStream.rangeClosed(0, len)
                .reduce(0, Integer::sum);
    }
}
